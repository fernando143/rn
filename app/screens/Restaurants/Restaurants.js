import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import ActionButton from "react-native-action-button";
import * as firebase from "firebase";

const Restaurants = props => {
  const { navigation } = props;
  const [user, setUser] = useState(null);

  useEffect(() => {
    firebase.auth().onAuthStateChanged(userInfo => setUser(userInfo));
  }, []);

  return (
    <View style={styles.viewBody}>
      <Text>Estamos en Restaurantes.</Text>

      {user && (
        <ActionButton
          buttonColor="#00a680"
          onPress={() => navigation.navigate("AddRestaurant")}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({ viewBody: { flex: 1 } });

export default Restaurants;
