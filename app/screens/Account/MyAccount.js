import React, { useState, useEffect } from "react";
import * as firebase from "firebase";
import Loading from "../../components/Loading";
import UserGuest from "../Account/UserGuest";
import UserLogged from "../Account/UserLogged";

import { View, Text } from "react-native";

const MyAccount = () => {
  const [login, setLogin] = useState(null);

  useEffect(() => {
    firebase.auth().onAuthStateChanged(user => {
      setTimeout(() => {
        setLogin(!!user);
      }, 500);
    });
  }, []);

  if (login === null) {
    return <Loading isVisible={true} text="Cargando..." />;
  }

  return login ? <UserLogged /> : <UserGuest />;
};

export default MyAccount;
