import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Avatar } from "react-native-elements";
import * as firebase from "firebase";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";

const InfoUser = props => {
  const {
    userInfo: { uid, displayName, email, photoURL },
    setReloadData,
    reloadData,
    toastRef,
    setIsLoading,
    setTextLoading
  } = props;

  const changeAvatar = async () => {
    const resultPermissions = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );

    if (resultPermissions.granted) {
      console.log("permisos aceptados");
      const result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3]
      });

      if (result.cancelled) {
        toastRef.current.show(
          "Has cerrado la galeria sin seleccionar ninguna imagen"
        );
      } else {
        uploadImage(result.uri, uid).then(() => {
          updatePhotoUrl(uid);
        });
      }
    } else {
      toastRef.current.show("Es necesario aceptar los permisos de la galeria");
    }
  };

  const uploadImage = async (uri, nameImage) => {
    setTextLoading("Actualizando avatar");
    setIsLoading(true);
    const response = await fetch(uri);
    const blob = await response.blob();

    const ref = firebase
      .storage()
      .ref()
      .child(`avatar/${nameImage}`);

    return ref.put(blob);
  };

  const updatePhotoUrl = uid => {
    firebase
      .storage()
      .ref(`avatar/${uid}`)
      .getDownloadURL()
      .then(async data => {
        console.log(`downloadURL: ${data}`);
        const update = { photoURL: data };
        firebase
          .auth()
          .currentUser.updateProfile(update)
          .then(() => {
            console.log("photoURL actualizada");
            setReloadData(reloadData === 0 ? 1 : 0);
            setIsLoading(false);
          })
          .catch(error => {
            console.log("error al actualizar photoURL");
          });
      })
      .catch(error => {
        toastRef.current.show("Error al recuperar el avatar del servidor");
        console.log(error);
      });
  };

  return (
    <View style={styles.viewUserInfo}>
      <Avatar
        rounded
        size="large"
        showEditButton
        onEditPress={changeAvatar}
        containerStyle={styles.userInfoAvatar}
        source={{
          uri: photoURL
            ? photoURL
            : "https://api.adorable.io/avatars/285/abott@adorable.png"
        }}
      />
      <View>
        <Text style={styles.displayName}>
          {displayName ? displayName : "Anónimo"}
        </Text>
        <Text>{email}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  viewUserInfo: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "#f2f2f2",
    paddingTop: 20,
    paddingBottom: 30
  },
  userInfoAvatar: {
    marginRight: 20
  },
  displayName: { fontWeight: "bold" }
});

export default InfoUser;
