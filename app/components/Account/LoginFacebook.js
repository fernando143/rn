import React, { useState, useEffect } from "react";
import { SocialIcon } from "react-native-elements";
import * as firebase from "firebase";
import * as Facebook from "expo-facebook";
import { FacebookApi } from "../../utils/Social";
import Loading from "../Loading";

const LoginFacebook = props => {
  const { toastRef, navigation } = props;
  const { application_id, permissions } = FacebookApi;
  const [isFacebookInitialized, setIsFacebookInitialized] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    Facebook.initializeAsync(application_id)
      .then(() => {
        setIsFacebookInitialized(true);
      })
      .catch(error => {
        setIsFacebookInitialized(false);
      });
  }, []);

  const login = async () => {
    if (!isFacebookInitialized) return false;

    const { type, token } = await Facebook.logInWithReadPermissionsAsync(
      application_id,
      {
        permissions
      }
    );

    switch (type) {
      case "success":
        const credentials = firebase.auth.FacebookAuthProvider.credential(
          token
        );
        setIsLoading(true);

        await firebase
          .auth()
          .signInWithCredential(credentials)
          .then(data => {
            navigation.navigate("MyAccount");
          })
          .catch(error => {
            toastRef.current.show(
              "Error accediendo con Facebook. Inténtelo más tarde"
            );
          });
        break;
      case "cancel":
        toastRef.current.show("inicio de sesion cancelado");
        break;
      default:
        toastRef.current.show("error desconocido");
        break;
    }
    setIsLoading(false);
  };

  return (
    <>
      <SocialIcon
        title="Iniciar sesión con Facebook"
        button
        type="facebook"
        onPress={login}
      />
      <Loading isVisible={isLoading} text="Iniciando sesión" />
    </>
  );
};

export default LoginFacebook;
