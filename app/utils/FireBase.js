import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyC7Y7fJL782FjSbLzFN616fOE0zZ_kYcL4",
  authDomain: "tenedores-2.firebaseapp.com",
  databaseURL: "https://tenedores-2.firebaseio.com",
  projectId: "tenedores-2",
  storageBucket: "tenedores-2.appspot.com",
  messagingSenderId: "865602009697",
  appId: "1:865602009697:web:9d3cfc158604914a238820"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
