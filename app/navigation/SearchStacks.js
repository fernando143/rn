import { createStackNavigator } from "react-navigation-stack";
import SearchScreen from "../screens/Search.js";

const SearchScreenStacks = createStackNavigator({
  TopRestaurants: {
    screen: SearchScreen,
    navigationOptions: () => ({
      title: "Busca tu restaurante"
    })
  }
});

export default SearchScreenStacks;
